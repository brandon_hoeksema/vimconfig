" allow backspacing over everything in insert mode
set backspace=indent,eol,start

if has("vms")
    set nobackup        " do not keep a backup file, use versions instead
else
    set backup      " keep a backup file
endif

set history=50      " keep 50 lines of command line history
set ruler       " show the cursor position all the time
set showcmd     " display incomplete commands
set incsearch       " do incremental searching

" In many terminal emulators the mouse works just fine, thus enable it.
if has('mouse')
    set mouse=a
endif

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
    syntax on
    set hlsearch
endif

" Only do this part when compiled with support for autocommands.
if has("autocmd")

    " Enable file type detection.
    " Use the default filetype settings, so that mail gets 'tw' set to 72,
    " 'cindent' is on in C files, etc.
    " Also load indent files, to automatically do language-dependent indenting.
    filetype plugin indent on

    " Put these in an autocmd group, so that we can delete them easily.
    augroup vimrcEx
        au!

        " For all text files set 'textwidth' to 78 characters.
        autocmd FileType text setlocal textwidth=78

        " When editing a file, always jump to the last known cursor position.
        " Don't do it when the position is invalid or when inside an event handler
        " (happens when dropping a file on gvim).
        " Also don't do it when the mark is in the first line, that is the default
        " position when opening a file.
        autocmd BufReadPost *
                    \ if line("'\"") > 1 && line("'\"") <= line("$") |
                    \   exe "normal! g`\"" |
                    \ endif

    augroup END

else

    set autoindent      " always set autoindenting on

endif " has("autocmd")

map <F5> :w<bar>!python3 "%"<cr>
set directory=~/.vim/swap
set autochdir

hi Normal guifg=white guibg=black
set background=dark

set foldmethod=marker

set noswapfile

set expandtab
set shiftwidth=4
set tabstop=4
set softtabstop=4
set mouse=
set nobackup

"MAPPINGS {{{

"Want to use enter or space as leader
let mapleader = "\<space>"

"NORMAL {{{
"Split view with vimrc
:nnoremap <leader>ev :split $MYVIMRC<cr>

"Close and enforce new vimrc
:nnoremap <leader>sv :wq<cr>:source $MYVIMRC<cr>

"Remove dependency on arrows for movement
:nnoremap <up> <nop>
:nnoremap <down> <nop>
:nnoremap <left> <nop>
:nnoremap <right> <nop>
"}}}

"INSERT {{{
"Insert esc to jk and disable esc
:inoremap jk <esc>
:vnoremap ` <esc>
"}}}
"}}}

"CHANGE CWD {{{
:autocmd BufRead,BufNewFile * cd %:p:h
:autocmd BufEnter * cd %:p:h
"}}}
"}}}

vnoremap = :!bc -l ~/bc_functions/all.bc<cr>

execute pathogen#infect()
filetype plugin on
filetype plugin indent on
autocmd BufNewFile,BufRead *.cl set syntax=cpp

:colorscheme farout_trans

:command MC :execute "/^\\(<<<<\\|====\\|>>>>\\)"
:command -range JSON <line1>,<line2>!python3 -m json.tool

function! DedupeMe() range
    execute ":".a:firstline.",".a:lastline."s/^/dedupe-me-plz"
    execute ":".a:firstline.",".a:lastline."g/^dedupe-me-plz/m0"
    execute ":g/^\\(dedupe-me-plz.\\+\\)\\ze\\n\\%(.*\\n\\)*\\1$/d"
    execute ":g/^dedupe-me-plz/m$"
    execute ":g/^dedupe-me-plz/m".(a:firstline-1)
    execute ":%s/^dedupe-me-plz//"
endfunction

:command -range DeDupe <line1>,<line2>call DedupeMe()

function! HtmlFormatMe() range
    let init=line('$')
    execute ":".a:firstline.",".a:lastline."s/></>\\r</g"
    let final=line('$')
    let diff=(final-init)
    execute "normal! ".a:firstline."G"
    execute "normal! =".diff."j"
endfunction

:command -range HtmlFormat <line1>,<line2>call HtmlFormatMe()

au BufRead,BufNewFile *.ts   setfiletype typescript

aug CSV_Editing
    au!
    au BufRead,BufWritePost *.csv :%ArrangeColumn
    au BufRead,BufWritePost *.csv :Header
    au BufRead,BufWritePost *.csv set cursorline
    au BufRead,BufWritePost *.csv hi  Search             ctermfg=187        ctermbg=NONE        cterm=NONE
    au BufWritePre *.csv :%UnArrangeColumn
aug end

au VimLeavePre * if v:this_session != '' | exec "mks! " . v:this_session | endif
